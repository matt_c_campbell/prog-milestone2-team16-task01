﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*

    Author:         Matt. Campbell [9989880]
    Date Created:   26th September, 2016.

    Task 01:        Date calculator.

*/

namespace prog_milestone2_task01
{

    class Program
    {

/*————————————————————————————————————————————————————————————————————————————————————————————————————————————————————*/

        /* Global Variables */

        /* GUI strings. */
        static string gui_divline = "\n▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄";
        static string gui_sectiondiv = "\n------------------------------------------------------------------------------------------------------------------------";

        /* GUI messages. */
        static string msg_tryagain = "\n------------------------------------------------------------------------------------------------------------------------\nPress any key to try again . . .";
        static string msg_exit = "\n------------------------------------------------------------------------------------------------------------------------\nPress any key to exit program . . .";

        /* Dates. */
        static System.DateTime today = DateTime.Now;

/*————————————————————————————————————————————————————————————————————————————————————————————————————————————————————*/

        static void Main(string[] args)
        {
            /*CurrentDate();

            Console.WriteLine(gui_sectiondiv);

            Birth_Days();

            Console.WriteLine(gui_sectiondiv);*/

            Years_Calculator();

            Exit();
        }

/*————————————————————————————————————————————————————————————————————————————————————————————————————————————————————*/

        /* Display today's date. */
        static void CurrentDate()
        {
            Console.WriteLine($"Today's date is " + today.ToString("dd/MM/yyyy") + ".");
        }

/*————————————————————————————————————————————————————————————————————————————————————————————————————————————————————*/

        /* Tell user that they are X days old based on their input. */
        static void Birth_Days()
        {
            bool dob_correctformat = false;
            var dob = new DateTime();

            /* Error correction for user input. */
            do
            {
                Console.WriteLine("Enter your date of birth (DoB) in dd/mm/yyyy format:");
                var input = Console.ReadLine();
                var temp1 = DateTime.TryParse(input, out dob);

                /* Is user's answer a date? */
                if (temp1 == false)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine($"\nERROR: I expected a date in dd/mm/yyyy format but you answered '{input}'.");
                    Console.ForegroundColor = ConsoleColor.Gray;

                    Console.WriteLine(msg_tryagain);

                    Console.ReadKey();
                    Console.Clear();
                }
                /* Is that date invalid (we are asking for a date of birth, so date should be in the past not in the future)? */
                else if (dob > today.AddDays(-1))
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine($"\nERROR: I expected a date before today (" + today.ToString("dd/MM/yyyy") + ") but you answered '" + dob.ToString("dd/MM/yyyy") + "'.");
                    Console.ForegroundColor = ConsoleColor.Gray;

                    Console.WriteLine(msg_tryagain);

                    Console.ReadKey();
                    Console.Clear();
                }
                else
                {
                    dob_correctformat = true;
                }
                
            } while (dob_correctformat == false);

            /* Outputs method result to screen. */
            Console.WriteLine($"\nYou were born on " + dob.ToString("dd/MM/yyyy") + " and you are " + (today.Date - dob.Date).Days + " days old.");
        }

/*————————————————————————————————————————————————————————————————————————————————————————————————————————————————————*/

        /* Tells user how many days are in X years. */
        static void Years_Calculator()
        {
            bool yc_correctformat = false;
            System.DateTime from_beginning = new DateTime(1, 1, 1);
            /* NOTE: The maximum value for data type Int is 2,147,483,647. Any value higher than this will be treated as being invalid by int.TryParse().
            int years = 0; */
            long years = 0;
            var days = 0;

            /* Error correction for user input. */
            do
            {
                Console.WriteLine("Enter a number higher than zero (0):");
                var input = Console.ReadLine();
                var temp = long.TryParse(input, out years); 

                /* Is user's answer a number? */
                if (temp == false)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine($"\nERROR: '{years}' is not a whole number.");
                    Console.ForegroundColor = ConsoleColor.Gray;

                    Console.WriteLine(msg_tryagain);

                    Console.ReadKey();
                    Console.Clear();
                }
                /* Is that number invalid (below or equal to zero (0))? */
                else if (years < 1)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine($"\nERROR: I expected a whole number greater than 0, but you answered '{input}'.");
                    Console.ForegroundColor = ConsoleColor.Gray;

                    Console.WriteLine(msg_tryagain);

                    Console.ReadKey();
                    Console.Clear();
                }
                /* If years is more than 9999, program will crash with an 'ArguementOutOfRangeException' error. */
                else if (years > 9999)
                {
                    years = 9999;

                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine($"\nN.B.: Your answer '{input}' has been changed to the max accepted DateTime() value of 9999.");
                    Console.ForegroundColor = ConsoleColor.Gray;

                    Console.WriteLine("\n------------------------------------------------------------------------------------------------------------------------\nPress any key to continue . . .");

                    Console.ReadKey();
                    Console.Clear();
                    yc_correctformat = true;
                }
                else
                {
                    yc_correctformat = true;
                }
            } while (yc_correctformat == false);

            /* Checks if year is leap year, and adds days to 'days'. */
            for(var i = 0; i < years; i++)
            {
                /* Adds years to date. */
                var a = from_beginning.AddYears(i);
                
                /* This line is used for debugging
                Console.WriteLine($"a = {a}"); */
                
                /* Removes days and months. */
                var b = a.Year;
                
                /* Checks if year is a leap year. */
                if (DateTime.IsLeapYear(b))
                {
                    days += 366;
                }
                else
                {
                    days += 365;
                }
            }

            /* Outputs method result to screen. */
            Console.WriteLine($"\nThere are {days} days in {years} years.");
        }

/*————————————————————————————————————————————————————————————————————————————————————————————————————————————————————*/

        /* Pause the program on screen until the user is ready to exit. */
        static void Exit()
        {
            Console.WriteLine(msg_exit);
            Console.ReadKey();
        }

/*————————————————————————————————————————————————————————————————————————————————————————————————————————————————————*/

    }

}